﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;

namespace StiriVVrsto
{

    // podatki za cfg datoteko
    class config
    {
        public config(string n, string v) { Value = v; Name = n; }

        public string Value { get; set; } // tekst grafičnega elementa
        public string Name { get; set; } // ime elementa
    }

    class Configuration
    {
        
        // Naloži datoteko tipa *.cfg
        // Vrne seznam configuracij (oz. null ob napaki)
        public static List<config> LoadCFG(string cfg)
        {
            List<config> config = new List<config>();
            try
            {
                // iz datoteke preberemo vse vrstice
                string[] data = File.ReadAllLines(cfg);

                // iteracija preko vrstic
                foreach (string s in data)
                {
                    // če je vrstica prazna ali pa komentar gremo na naslednjo
                    if (s == "" || s.Substring(0, 2).Equals("//"))
                        continue;

                    // razdelimo niz, da dobimo ven "Value" in "Name"
                    string[] x = s.Split('|');
                    string name = x[0].Substring(1, x[0].Length - 2);
                    string value = x[1].Substring(1, x[1].Length - 2);
                    config.Add(new config(name, value));
                }
                return config;
            }
            catch (Exception e)
            {
            }
            return null;
        }

        // Ekstrahira datoteke iz zip arhiva v Windowsovo mapo TEMP
        // funkcije so uporabljene iz knjižnjice DotNetZip
        public static bool LoadZip(string path)
        {
            if (!File.Exists(path)) return false;

            using (ZipFile zip = ZipFile.Read(path))
            {
                try
                {
                    zip.ExtractAll(Environment.GetEnvironmentVariable("TEMP"), ExtractExistingFileAction.DoNotOverwrite);
                }
                catch (Exception e) { }
            }
            return true;
        }

    }
}

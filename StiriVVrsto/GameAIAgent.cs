﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StiriVVrsto
{
    // ta razred predstvalja AI - umetno inteligenco
    // igramo lahko proti agentu s štirimi zahtevnostmi (TODO)
    class GameAIAgent
    {
        // TODO - različne zahtevnosti
        public GameAIAgent(int diffuculty)
        {
            r = new Random();
        }

        // vrne naslednjo potezo glede na težavnost
        public int NextMove(Game gameRef) 
        {
            getPossibleMoves(gameRef);
            return possibleMoves[r.Next(possibleMoves.Count)];
        }

        // shrani vse možne poteze za dano igro
        private void getPossibleMoves(Game gameRef)
        {
            possibleMoves.Clear();
            for (int i = 0; i < 7; i++)
            {
                int row = gameRef.game.Data.GetRow(i);
                if (row > -1)
                {
                    possibleMoves.Add(i);
                }
            }
        }

        List<int> possibleMoves = new List<int>();
        Random r;
    }
}

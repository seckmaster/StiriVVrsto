﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace StiriVVrsto
{

    public partial class Form1 : Form
    {
        // igra
        Game game;

        // za risanje
        Bitmap bmp;
        Graphics g;

        // lokacije slik in imena slik
        string location = Environment.GetEnvironmentVariable("TEMP");
        string bg = @"\bg_classic.png";
        string d1 = @"\disc1_classic.png";
        string d2 = @"\disc2_classic.png";

        // nizi za prikaz sporočil ob napaki
        string dataNotFoundError = "";
        string imagesNotFoundError = "";

        // sliki za krogce
        Image disc1, disc2;

        public Form1()
        {
            InitializeComponent();

            // default lokacija cfg datoteke
            string cfg = "../../config.cfg";
            LoadUIFromConfigFile(cfg);

            // ekstrahira teksture iz privzetega argiva
            bool success = Configuration.LoadZip("../../classic.zip");

            if (success) ReloadImage();
            else
            {
                // če je prišlo do napake pri branju tekstur
                MessageBox.Show(dataNotFoundError);
                Application.Exit();
            }

        }

        // klik na picturebox
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (game != null && !game.game.End)
            {
                // naslednja poteza
                game.NextMove((e as MouseEventArgs).X);

                // posodobimo picturebox
                pictureBox1.Image = bmp;
            }
        }

        // klik Nova igra
        private void button1_Click(object sender, EventArgs e)
        {
            // ugotovimo kdo naj začne igro glede na radiobuttone
            Begins b = 0;

            if (radioButton1.Checked)      b = Begins.Red;
            else if (radioButton2.Checked) b = Begins.Yellow;
            else if (radioButton3.Checked) b = Begins.RY;

            // če objekt igra še ni bil kreiran
            if (game == null)
            {
                Versus versus = radioButton7.Checked ? Versus.Player : Versus.Ai;
                // kreiramo objekt
                game = new Game(g, b, versus, disc1, disc2, pictureBox1.Width, pictureBox1.Height);
            }

            // začni novo igro
            game.game.starts = b;
            game.game.StartNewGame();

            // zbriši picturebox
            ReloadImage();
        }

        // naloži UI iz cfg datoteke
        private void LoadUIFromConfigFile(string cfg)
        {
            try
            {
                List<config> config = Configuration.LoadCFG(cfg);
                foreach (config c in config)
                {
                    if      (c.Name.Equals("Form")) this.Text = c.Value;

                    else if (c.Name.Equals("Button 1")) button1.Text = c.Value;
                    else if (c.Name.Equals("Button 5")) button5.Text = c.Value;
                    else if (c.Name.Equals("save")) button2.Text = c.Value;
                    else if (c.Name.Equals("load")) button3.Text = c.Value;
                    else if (c.Name.Equals("nextMove")) button4.Text = c.Value;

                    else if (c.Name.Equals("Radio 1")) radioButton1.Text = c.Value;
                    else if (c.Name.Equals("Radio 2")) radioButton2.Text = c.Value;
                    else if (c.Name.Equals("Radio 3")) radioButton3.Text = c.Value;
                    else if (c.Name.Equals("Radio 7")) radioButton7.Text = c.Value;
                    else if (c.Name.Equals("Radio 8")) radioButton8.Text = c.Value;

                    else if (c.Name.Equals("Label 1")) label1.Text = c.Value;

                    else if (c.Name.Equals("dataError")) dataNotFoundError = c.Value;
                    else if (c.Name.Equals("imagesError")) imagesNotFoundError = c.Value;

                    else if (c.Name.Equals("groupBox1")) groupBox1.Text = c.Value;
                    else if (c.Name.Equals("groupBox2")) groupBox2.Text = c.Value;
                    else if (c.Name.Equals("groupBox3")) groupBox3.Text = c.Value;
                    else if (c.Name.Equals("groupBox4")) groupBox4.Text = c.Value;

                    else if (c.Name.Equals("dialogFilter")) 
                    {
                        saveFileDialog1.Filter = c.Value.Replace('#', '|');
                        openFileDialog1.Filter = c.Value.Replace('#', '|');
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error loading cfg! Game will close now!");
                Application.Exit();
            }
        }

        // ponovno naloži vse slike
        private void ReloadImage()
        {
            try
            {
                // naložimo slike iz datotek
                Image b = Image.FromFile(location + bg);

                pictureBox1.Image = b;

                disc1 = Image.FromFile(location + d1);
                disc2 = Image.FromFile(location + d2);

                // če smo v igri, posodobimo slike
                if (game != null)
                {
                    game.disc1 = disc1;
                    game.disc2 = disc2;
                }

                bmp = new Bitmap(pictureBox1.Image);
                g = Graphics.FromImage(bmp);
            }
            catch (Exception e)
            {
                MessageBox.Show(imagesNotFoundError);
                Application.Exit();
            }
        }

        // shrani igro
        private void button2_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                game.camera.SaveToFile(saveFileDialog1.FileName);
            }
        }

        // naloži igro
        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                game.camera.LoadFromFile(openFileDialog1.FileName);
                game = new Game(g, game.camera.v, Versus.Player, disc1, disc2, pictureBox1.Width, pictureBox1.Height); 

                ReloadImage();
                button4.Enabled = true;

                next = 0;
            }
        }

        // naslednja poteza pri prikazovanju igre iz datoteke
        int next = 0; // naslednja poteza iz recorderja
        private void button4_Click(object sender, EventArgs e)
        {
            if (next < game.camera.columns.Count)
            {
                // naredi naslednjo potezo
                game.NextMove(game.camera.columns[next++]);
                pictureBox1.Image = bmp;

                // če je konec posnetka onemogoči gumb
                if (next == game.camera.columns.Count)
                    button4.Enabled = false;
            }
        }

        // sprememba teme
        private void button5_Click(object sender, EventArgs e)
        {
            // katera tema je izbrana
            if (radioButton4.Checked)
            {
                bg = @"\bg_classic.png";
                d1 = @"\disc1_classic.png";
                d2 = @"\disc2_classic.png";
                Configuration.LoadZip("../../classic.zip");
            }

            else if (radioButton5.Checked)
            {
                bg = @"\bg_plastic.png";
                d1 = @"\disc1_plastic.png";
                d2 = @"\disc2_plastic.png";
                Configuration.LoadZip("../../plastic.zip");
            }

            else if (radioButton6.Checked)
            {
                bg = @"\bg_metal.png";
                d1 = @"\disc1_metal.png";
                d2 = @"\disc2_metal.png";
                Configuration.LoadZip("../../metal.zip");
            }
            // posodobimo teksture z novimi slikami
            ReloadImage();

            if (game != null)
            {
                // resetiramo igro
                game.game.StartNewGame();
                // zmanjšamo števec, če prejšnja igra ni dokončana
                if(!game.game.End)
                    game.game.GamesPlayed--;
            }
        }

    }
}

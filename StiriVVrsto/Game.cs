﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace StiriVVrsto
{

    // Proti komu lahko igramo (drug igralec ali pa bot)
    enum Versus
    {
        Player, Ai
    }

    // ta razred vsebuje vse podatki in metode za simulacijo igre 4 v vrsto
    class Game
    {
        // konstruktor - povemo kdo naj začne, proti komu igrame, sliki za diska, 
        // in pa širino in višino pictureboxa (da lahko pravilno rišemo slike)
        public Game(Graphics g, Begins b, Versus v, Image disc1, Image disc2, int width, int height)
        {
            // create new game handler
            game = new GameHandler(b);

            // izračunamo, koliko je velik vsak stolpec in vrstica
            OFFSET_X = width / game.Data.Width;
            OFFSET_Y = height / game.Data.Height;

            this.disc1 = disc1;
            this.disc2 = disc2;

            versus = v;

            camera = new GameRecorder();
        }

        // naredi naslednjo potezo
        // vrne stolpec, na katerega smo kliknili
        public void NextMove(int column)
        {
            // če je igre konec ne naredimo nič
            if (!game.End)
            {
                // vrstica, ki je v tem stolpcu najnižja nezasedena
                int row = game.ContinueGame(column);

                // če je igre konec ne rabimo pregledovat dokler potez ni vsaj 7
                if (game.Counter > 6)
                {
                    bool end = new GameLogic(game.Data, 4).checkForWinner();
                    if (end)
                    {
                        game.ExitGame();
                    }
                }

                // nariše sliko diska na lokaciji row | column
                int x = OFFSET_Y + 8;
                Draw.DrawImage(
                    g,
                    column * x + 35, // x
                    row * OFFSET_Y + 9,  // y
                    80, 80,
                    (game.nextPlayer == Begins.Red) ? disc2 : disc1
                );

                // ob koncu igre prikažemo message box s tekstom kdo je zmagal
                if (game.End)
                {
                    // ali je zmagal rdeči, rumen oz. neodločeno
                    string winner = "";
                    if (game.Counter == game.Data.Width * game.Data.Height) winner = "Neodločen izid.";
                    else if (game.nextPlayer == Begins.Red) winner = "Zmagal je rumeni!";
                    else if (game.nextPlayer == Begins.Yellow) winner = "Zmagal je rdeči!";

                    CustomMessageBox.Show("Konec igre! " + winner, "Konec igre!");
                }

            }
        }

        // ta funkcija kliče zgornjo funkcijo NextMove
        // mouse_click_x predstavlja x koordinato kamor smo kliknili na picturebox
        public void NextMove(float mouse_click_x)
        {
            // iz koordinate preračunamo kater stolpec je to
            int column = ToColumn((int)mouse_click_x);

            // kličemo funkcijo z izračunanim stolpec
            NextMove(column);
        }

        // pretvori iz koordinate v stolpec
        private int ToColumn(int mouse_click_x)
        {
            return mouse_click_x / OFFSET_X;
        }

        // data for game
        public GameHandler game { get; private set; }

        // proti komu igramo
        Versus versus;

        // velikost stolpca in vrstice
        int OFFSET_X, OFFSET_Y;

        // sliki za diske
        public Image disc1 { get; set; }
        public Image disc2 { get; set; }
        public Graphics g { get; private set; }

        // game recorder
        public GameRecorder camera { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StiriVVrsto
{
    
    internal partial class CustomMessageForm : Form
    {
        public CustomMessageForm()
        {
            InitializeComponent();
        }

        public CustomMessageForm(string text, string name)
        {
            InitializeComponent();
            // lokacija MessageBoxa naj bo na sredini
            StartPosition = FormStartPosition.CenterParent;
            // odstrani rob okna
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            // nastavimo opacity (prosojnost) okna
            Opacity = 0.8;

            label1.Text = text;
            Text = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

    public static class CustomMessageBox
    {
        public static void Show(string text, string name)
        {
            // using construct ensures the resources are freed when form is closed
            using (var form = new CustomMessageForm(text, name))
            {
                form.ShowDialog();
            }
        }
    }
}

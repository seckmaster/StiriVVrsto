﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace StiriVVrsto
{
    
    class GameRecorder
    {
        // seznam, ki nam pove zaporedje stolpcev
        public List<int> columns { get; set; }
        // kdo je začel igro
        public Begins v { get; set; }

        public GameRecorder()
        {
            columns = new List<int>();
        }

        // shrani podatke iz columns v datoteko (binarno)
        public void SaveToFile(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);

            bw.Write((int)v);
            bw.Write(columns.Count);
            foreach (int i in columns)
                bw.Write(i);

            bw.Close();
            fs.Close();
        }

        // naloži podatke iz datoteke
        public void LoadFromFile(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryReader bw = new BinaryReader(fs);

            // počistimo seznam da ne pride do napake
            columns.Clear();

            v = (Begins)bw.ReadInt32();
            int n = bw.ReadInt32();
            for (int i = 0; i < n; i++)
                columns.Add(bw.ReadInt32());

            bw.Close();
            fs.Close();
        }
    }
}

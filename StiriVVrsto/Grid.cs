﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StiriVVrsto
{
    // možnosti za disk (rdeč, rumen, prazen)
    enum DiscColor
    {
        Red, Yellow, Empty
    }

    class Disc
    {
        public Disc()
        {
            // privzeto je disk prazen
            Value = DiscColor.Empty;
        }

        public DiscColor Value { set; get; }
    }

    // 7 * 6 grid
    class Grid
    {
        // kreira tabelo 6 * 7 (standard 4 v vrsto)
        public Grid()
        {
            Width = 7;
            Height = 6;
            Field = new Disc[Height, Width];
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Field[i, j] = new Disc();
                }
            }
        }

        // v tabelo vstavi disk v dan stolpec
        public int Insert(int column, DiscColor c)
        {
            // če ta stolpec ni "pravilen" vrne -1
            if (IsLegal(column))
            {
                // vrstica v tem stolpcu, ki je prazna
                int index = GetRow(column);

                // v tabelo vstavi disk
                if (index != -1)
                {
                    Field[index, column].Value = c;
                    return index;
                }
            }
            return -1;
        }

        // ali je stolpec legalen
        public bool IsLegal(int column)
        {
            return (column > 0 || column < Width);
        }

        // vrne -1 če je stolpec povn
        public int GetRow(int column)
        {
            for (int i = Height - 1; i >= 0; i--)
            {
                if (Field[i, column].Value == DiscColor.Empty)
                {
                    return i;
                }
            }
            return -1;
        }

        // data
        public int Width { get; private set; }
        public int Height { get; private set; }
        public Disc[,] Field { get; private set; }
    }
}

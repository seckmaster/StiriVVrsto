﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace StiriVVrsto
{
    class Draw
    {
        // static methods for drawing --------------------------------------------------------------

        // nariše zapolnjen krog na dani lokaciji z izbrano barvo
        public static void DrawFilledCircle(Graphics g, int x, int y, int size, Color c)
        {
            using (Pen p = new Pen(c))
            {
                g.FillEllipse(p.Brush, x, y, size, size);
            }
        }

        // nariše sliko na dani lokaciji
        public static void DrawImage(Graphics g, int x, int y, int width, int height, Image i)
        {
            g.DrawImage(i, new Rectangle(x, y, width, height));
            g.Flush();
        }

        // nariše črto
        public static void DrawLine(Graphics g, Point a, Point b, Color c, int width)
        {
            using (Pen p = new Pen(c, width))
            {
                g.DrawLine(p, a, b);
            }
        }
        // static methods for drawing --------------------------------------------------------------

    }
}

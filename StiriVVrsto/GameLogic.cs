﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Diagnostics;

namespace StiriVVrsto
{
    class GameLogic
    {
        Grid g;
        int n;

        public GameLogic(Grid g, int n) 
        {
            this.g = g;
            this.n = n;
        }

        public bool checkForWinner()
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    bool p = checkAt(i, j);
                    if (p) return p;
                }
            }
            return false;
        }

        // preveri, ali lahko na koordinati x y povežemo n diskov
        private bool checkAt(int x, int y) 
        {
            return (checkColumn(x, y) || checkRow(x, y) || checkDiagA(x, y) || checkDiagB(x, y));
        }

        // preveri vrstico
        private bool checkColumn(int x, int y)
        {
            List<int> l = new List<int>();
            if (y < (7 - n + 1))
            {
                for (int i = y; i < y + n; i++)
                {
                    if (g.Field[x, i].Value != DiscColor.Empty)
                    {
                        l.Add((int)g.Field[x, i].Value);
                    }
                    else break;
                }

                if (l.Count == n && AreSame(l)) return true;
                l.Clear();
            }
            return false;
        }

        // preveri stolpec
        private bool checkRow(int x, int y)
        {
            List<int> l = new List<int>();
            if (x < (6 - n + 1))
            {
                for (int i = x; i < x + n; i++)
                {
                    if (g.Field[i, y].Value != DiscColor.Empty)
                    {
                        l.Add((int)g.Field[i, y].Value);
                    }
                    else break;
                }

                if (l.Count == n && AreSame(l)) return true;
            }
            return false;
        }

        // preveri prvo diagonalo
        private bool checkDiagA(int x, int y)
        {
            List<int> l = new List<int>();
            if (x > (6 - n) && y < (7 - n + 1))
            {
                int count = 0;
                for (int i = x, j = y; count < n; i--, j++, count++)
                {
                    if (g.Field[i, j].Value != DiscColor.Empty)
                    {
                        l.Add((int)g.Field[i, j].Value);
                    }
                    else break;
                }

                if (l.Count == n && AreSame(l)) return true;
            }
            return false;
        }

        // preveri drugo diagonalo
        private bool checkDiagB(int x, int y)
        {
            List<int> l = new List<int>();
            if (x > (6 - n) && y >= (7 - n))
            {
                int count = 0;
                for (int i = x, j = y; count < n; i--, j--, count++)
                {
                    if (g.Field[i, j].Value != DiscColor.Empty)
                    {
                        l.Add((int)g.Field[i, j].Value);
                    }
                    else break;
                }

                if (l.Count == n && AreSame(l)) return true;
            }
            return false;
        }

        // ali so vsi elementi v seznamu enaki
        private bool AreSame(List<int> x)
        {
            if (x.Count == 0) return false;

            int pr = x[0];
            foreach (int i in x)
            {
                if (i != 2 && i == pr) pr = i;
                else return false;
            }
            return true;
        }

    }
}

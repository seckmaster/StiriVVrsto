﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace StiriVVrsto
{
    // kdo začne igro (rdeč, rumen ali pa izmenično)
    enum Begins
    {
        Red, Yellow, RY
    }

    // vsebuje tri glavne funkcije za "hendlanje" igre - Nova igra, Nadaljuj, Končaj
    class GameHandler
    {
        // konstruktor - povemo kdo naj začne vsako igro
        public GameHandler(Begins starts)
        {
            this.starts = starts;
            nextPlayer = starts;
            if (nextPlayer == Begins.RY) nextPlayer = Begins.Red;

            Data = new Grid();
            Counter = 0;
            GamesPlayed = 0;
            End = false;
        }

        // začne novo igro in konča prejšnjo
        // resetira vse podatke na privzete vrednosti
        public void StartNewGame()
        {
            // reset grid
            Data = new Grid();

            // end previous game
            ExitGame();

            // who is first on next game
            if (starts == Begins.RY)
                nextPlayer = (GamesPlayed % 2 == 0) ? Begins.Red : Begins.Yellow;
            else nextPlayer = starts;

            End = false;
            Counter = 0;
            GamesPlayed++;
        }

        // nadaljuje igro
        public int ContinueGame(int column)
        {
            // če igre ni konec
            if (!End)
            {
                // kateri igralec je sedaj na potezi
                DiscColor c = (DiscColor)nextPlayer;
                // števec potez se poveča
                Counter++;

                nextPlayer = (nextPlayer == Begins.Red) ? Begins.Yellow : Begins.Red;

                // če je neodločeno
                if(Counter == Data.Width * Data.Height)
                    End = true;

                // vrne vrstico
                return Data.Insert(column, c);
            }
            return -1;
        }

        // konča igro
        public void ExitGame()
        {
            End = true;
        }

        // podatki za igro
        public bool End { get; set; }
        public Grid Data { get; private set; }
        public int Counter { get; private set; }
        public int GamesPlayed { get; set; }

        // kdo je naslednji na potezi
        public Begins nextPlayer { get; private set; }
        // kdo začne ob začetku igre
        public Begins starts { get; set; }
    }
}
